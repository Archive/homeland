// Homeland.cs - GNOME Homeland Security Threat Level Applet
//
// Robert Love	<rml@novell.com>
//
// Copyright (C) 2004 Novell, Inc.
//
// Licensed under the GNU GPL v2.  See COPYING.

using System;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

using Gtk;
using GtkSharp;
using Gnome;
using Egg;

public class Homeland {

	private static uint timeout = 20 * 60 * 1000;	// refresh every 20 minutes

	private static TrayIcon trayIcon;
	private static Label label;
	private static EventBox eb;
	private static Tooltips tooltip;	
	private static Gdk.Color red;
	private static Gdk.Color orange;
	private static Gdk.Color yellow;
	private static Gdk.Color blue;
	private static Gdk.Color green;

	private static string DownloadHomeland ()
	{
		WebClient wc = new WebClient ();
		byte[] bytestream = wc.DownloadData ("http://www.whitehouse.gov/homeland/");
		return Encoding.ASCII.GetString (bytestream);
	}

	private static string FindThreatLevel (string data)
	{
		Regex regex = new Regex (@"/homeland/images/threat/(?<level>.*)\.jpg");
		MatchCollection matches = regex.Matches (data);
		foreach (Match match in matches) {
			if (match.Length != 0)
				Console.WriteLine (match.Groups["level"].ToString ());
				return match.Groups["level"].ToString ();
		}
		return "error";
	}

	private static void ShowThreatLevel (string level)
	{
		switch (level) {
		case "severe":
			eb.ModifyBg (eb.State, red);
			tooltip.SetTip (eb, "Severe risk of terrorist attacks", null);
			break;
		case "high":
			eb.ModifyBg (eb.State, orange);
			tooltip.SetTip (eb, "High risk of terrorist attacks", null);
			break;
		case "elevated":
			eb.ModifyBg (eb.State, yellow);
			tooltip.SetTip (eb, "Significant risk of terrorist attacks", null);
			break;
		case "guarded":
			eb.ModifyBg (eb.State, blue);
			tooltip.SetTip (eb, "General risk of terrorist attacks", null);
			break;
		case "low":
			eb.ModifyBg (eb.State, green);
			tooltip.SetTip (eb, "Low risk of terrorist attacks", null);
			break;
		}

		label.Text = level;
	}

	private static bool HandleTimeout ()
	{
		string data = DownloadHomeland ();
		string match = FindThreatLevel (data);
		ShowThreatLevel (match);

		return true;
	}

	static void Main (string [] args)
	{
		Program homeland = new Program ("homeland", "0.0.1", Modules.UI, args);

		Gdk.Color.Parse ("red", ref red);
		Gdk.Color.Parse ("orange", ref orange);
		Gdk.Color.Parse ("yellow", ref yellow);
		Gdk.Color.Parse ("blue", ref blue);
		Gdk.Color.Parse ("green", ref green);

		label = new Label ();

		eb = new EventBox ();
		eb.Add (label);

		trayIcon = new TrayIcon ("Homeland");
		trayIcon.Add (eb);
		trayIcon.ShowAll ();

		tooltip = new Tooltips ();		

		HandleTimeout ();

		GLib.Timeout.Add (timeout, new GLib.TimeoutHandler (HandleTimeout));

		homeland.Run ();
	}

}
